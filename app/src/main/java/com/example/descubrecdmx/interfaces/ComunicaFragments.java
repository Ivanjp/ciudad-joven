package com.example.descubrecdmx.interfaces;

import com.example.descubrecdmx.ListEventos;
import com.example.descubrecdmx.ListLugares;

public interface ComunicaFragments {

    /**
     * Método que envia el objeto evento al fragment que muestra los detalles del evento
     */
    public void enviaEvento(ListEventos evento);

    /**
     * Método que envia el objeto lugar al fragment que muestra los detalles del lugar
     */
    public void enviaLugar(ListLugares lugar);
}
