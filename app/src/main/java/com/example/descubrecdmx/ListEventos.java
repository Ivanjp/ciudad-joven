package com.example.descubrecdmx;


import java.io.Serializable;

/**
 * Clase que modela el objeto evento
 */
public class ListEventos implements Serializable {

    //Atributos
    public String Nombre; //Nombre del evento
    public String Lugar; //Lugar donde se lleva a cabo
    public String Descripcion; //Descripcion corta del evento, que se muestra en la vista previa del evento
    public String DescripcionLarga;//Descripcion larga del evento
    public String costo;//Costo del evento
    public String fecha;//Fecha del evento
    public int idImage;//id de la imagen

    /**
     * Constructor
     */
    public ListEventos(String nombre, String lugar, String descripcion, String descripcionLarga, String costo, String fecha, int idImage) {
        Nombre = nombre;
        Lugar = lugar;
        Descripcion = descripcion;
        DescripcionLarga = descripcionLarga;
        this.costo = costo;
        this.fecha = fecha;
        this.idImage = idImage;
    }

    //Getters y Setters

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getLugar() {
        return Lugar;
    }

    public void setLugar(String lugar) {
        Lugar = lugar;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getCosto() {
        return costo;
    }

    public void setCosto(String costo) {
        this.costo = costo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getIdImage() {
        return idImage;
    }

    public void setIdImage(int idImage) {
        this.idImage = idImage;
    }

    public String getDescripcionLarga() {
        return DescripcionLarga;
    }

    public void setDescripcionLarga(String descripcionLarga) {
        DescripcionLarga = descripcionLarga;
    }
}
