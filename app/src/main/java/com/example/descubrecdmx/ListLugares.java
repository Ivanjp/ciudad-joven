package com.example.descubrecdmx;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

/**
 * Clase que modela el objeto lugar
 */
public class ListLugares implements Serializable {

    //Atributos
    public String nombre;//Nombre del lugar
    public String direccion;//Direccion donde se ubica el lugar
    public String horarios;//Horarios disponibles del lugar
    public String costo;//Costo del lugar
    public String contacto;//Info de contacto del lugar
    public String descripcion;//Descripcion del lugar
    public int idImage;//id de imagen
    public LatLng coord;//coordenadas


    /**
     * Constructor
     */
    public ListLugares(String nombre, String direccion, String horarios, String costo, String contacto, String descripcion, int idImage, LatLng coord) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.horarios = horarios;
        this.costo = costo;
        this.contacto = contacto;
        this.descripcion = descripcion;
        this.idImage = idImage;
        this.coord = coord;
    }

    //Getters y Setters
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getHorarios() {
        return horarios;
    }

    public void setHorarios(String horarios) {
        this.horarios = horarios;
    }

    public String getCosto() {
        return costo;
    }

    public void setCosto(String costo) {
        this.costo = costo;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdImage() {
        return idImage;
    }

    public void setIdImage(int idImage) {
        this.idImage = idImage;
    }

    public LatLng getCoord() {
        return coord;
    }

    public void setCoord(LatLng coord) {
        this.coord = coord;
    }
}
