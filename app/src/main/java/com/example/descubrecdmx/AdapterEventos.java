package com.example.descubrecdmx;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
/**
 * Adaptador para cargar datos de los eventos en un RecyclerViewer
 */
public class AdapterEventos extends RecyclerView.Adapter<AdapterEventos.ViewHolder> implements View.OnClickListener {

    private List<ListEventos> eventos;//Lista de los eventos que se van a cargar
    private View.OnClickListener listener;//Variable para evento Onclick en los elementos del recycler

    /**
     * Constructor
     */
    public AdapterEventos(List<ListEventos> eventos) {
        this.eventos = eventos;
    }

    /**
     * Método para enlazar el adaptador con el archivo XML con la estructura con la que se mostraran los datos
     */
    @Override
    public AdapterEventos.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.destacados_list,null,false);
        view.setOnClickListener(this);

        return new AdapterEventos.ViewHolder(view);
    }

    /**
     * Método para establecer la comunicacion entre el adaptador y la clase view holder
     */
    @Override
    public void onBindViewHolder(AdapterEventos.ViewHolder holder, int position) {
        holder.descripcion.setText(eventos.get(position).getDescripcion());
        holder.nombre.setText(eventos.get(position).getNombre());
        holder.image.setImageResource(eventos.get(position).getIdImage());
    }

    @Override
    public int getItemCount() {
        return eventos.size();
    }

    /**
     * Método para escuchar el evento del recycler
     */
    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    /**
     * Método para eventos en el recyclerr
     */
    @Override
    public void onClick(View v) {
        if(listener != null){
            listener.onClick(v);
        }
    }

    /**
     * Método que hace las instancias de los datos que se quieren mostrar a las referencias del recycler
     */
    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView image;//imagen del evento
        TextView nombre,descripcion;//datos que se mostraran como vista previa del evento

        ViewHolder(View itemView){
            super(itemView);
            //Mapeando los datos
            image = itemView.findViewById(R.id.idImagen);
            nombre = itemView.findViewById(R.id.idNombre);
            descripcion = itemView.findViewById(R.id.idInfo);
        }
    }
}
