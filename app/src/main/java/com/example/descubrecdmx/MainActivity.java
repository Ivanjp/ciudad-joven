package com.example.descubrecdmx;

import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Menu;

import com.example.descubrecdmx.fragments.cultura;
import com.example.descubrecdmx.fragments.destacados;
import com.example.descubrecdmx.fragments.detalleEvento;
import com.example.descubrecdmx.fragments.detalleLugar;
import com.example.descubrecdmx.fragments.lugares;
import com.example.descubrecdmx.fragments.ocio;
import com.example.descubrecdmx.interfaces.ComunicaFragments;
import com.google.android.material.navigation.NavigationView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, destacados.OnFragmentInteractionListener,cultura.OnFragmentInteractionListener
,ocio.OnFragmentInteractionListener,lugares.OnFragmentInteractionListener, ComunicaFragments {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                        this, drawer, toolbar,R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //Creamos el fragmento que se mostrará al iniciar la aplicación
        Fragment fragment=new destacados();
        getSupportFragmentManager().beginTransaction().add(R.id.nav_host_fragment_content_main,fragment).commit();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * Método que maneja el menú lateral, creando los fragments dependiendo de la sección que se escoja
     * @param item Item del menú
     */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Fragment miFragment=null;
        boolean fragmentSeleccionado=false;

        if (id == R.id.nav_destacados) {
            miFragment=new destacados();
            fragmentSeleccionado=true;
        } else if (id == R.id.nav_cultura) {
            miFragment=new cultura();
            fragmentSeleccionado=true;
        } else if (id == R.id.nav_ocio) {
            miFragment=new ocio();
            fragmentSeleccionado=true;
        } else if (id == R.id.nav_lugares) {
            miFragment=new lugares();
            fragmentSeleccionado=true;
        }

        if (fragmentSeleccionado==true){
            getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment_content_main,miFragment).commit();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    /**
     * Implementación del método de la interfaz para los eventos
     * @param evento Evento al que se le dio click
     */
    @Override
    public void enviaEvento(ListEventos evento) {
        Fragment fragment=new detalleEvento();//Creando el fragmento donde se mostraran los detalles
        Bundle bundleEnvia = new Bundle();//Bundle para transportar la información
        bundleEnvia.putSerializable("objeto",evento);//Mandando el evento
        fragment.setArguments(bundleEnvia);//Mandando el bundle al fragmento

        //Cargando el fragmento
        getSupportFragmentManager().beginTransaction().add(R.id.nav_host_fragment_content_main,fragment).addToBackStack(null).commit();
    }

    /**
     * Implementación del método de la interfaz para los lugares
     * @param lugar Lugar al que se le dio click
     */
    @Override
    public void enviaLugar(ListLugares lugar) {
        Fragment fragment=new detalleLugar();//Creando el fragmento donde se mostraran los detalles
        Bundle bundleEnvia = new Bundle();//Bundle para transportar la información
        bundleEnvia.putSerializable("objeto",lugar);//Mandando el evento
        fragment.setArguments(bundleEnvia);//Mandando el bundle al fragmento

        //Cargando el fragmento
        getSupportFragmentManager().beginTransaction().add(R.id.nav_host_fragment_content_main,fragment).addToBackStack(null).commit();
    }
}