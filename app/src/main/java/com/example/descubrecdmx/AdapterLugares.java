package com.example.descubrecdmx;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;



import java.util.List;
/**
 * Adaptador para cargar datos de los lugares en un RecyclerViewer
 */
public class AdapterLugares extends RecyclerView.Adapter<AdapterLugares.ViewHolder> implements View.OnClickListener {

    private List<ListLugares> lugares;//lista de los lugares que se van a cargar
    private View.OnClickListener listener;//Variable para evento Onclick en los elementos del recycler

    /**
     * Constructor
     */
    public AdapterLugares(List<ListLugares> lugares) {
        this.lugares = lugares;
    }


    /**
     * Método para enlazar el adaptador con el archivo XML con la estructura con la que se mostraran los datos
     */
    @Override
    public AdapterLugares.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lugares,null,false);
        view.setOnClickListener(this);//Ponemos al listener para detectar los eventos en este fragmento
        return new AdapterLugares.ViewHolder(view);
    }

    /**
     * Método para establecer la comunicacion entre el adaptador y la clase view holder
     */
    @Override
    public void onBindViewHolder(AdapterLugares.ViewHolder holder, int position) {
        holder.nombre.setText(lugares.get(position).getNombre());
        holder.image.setImageResource(lugares.get(position).getIdImage());
    }

    @Override
    public int getItemCount() {
        return lugares.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    /**
     * Método de escucha de eventos
     */
    @Override
    public void onClick(View v) {
        if(listener != null){
            listener.onClick(v);
        }
    }

    /**
     * Método que hace las instancias de los datos que se quieren mostrar a las referencias del recycler
     */
    public class ViewHolder extends RecyclerView.ViewHolder{
        //datos que se mostraran como vista previa del evento
        ImageView image;
        TextView nombre;

        ViewHolder(View itemView){
            super(itemView);
            //Mapeando los datos
            image = itemView.findViewById(R.id.idImagenLugar);
            nombre = itemView.findViewById(R.id.idNombreLugar);
        }
    }
}