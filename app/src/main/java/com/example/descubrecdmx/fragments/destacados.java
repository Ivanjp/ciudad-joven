package com.example.descubrecdmx.fragments;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.descubrecdmx.AdapterEventos;
import com.example.descubrecdmx.ListEventos;
import com.example.descubrecdmx.R;
import com.example.descubrecdmx.interfaces.ComunicaFragments;

import java.util.ArrayList;

/**
 * Fragmento para la sección de eventos destacados
 */
public class destacados extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ArrayList<ListEventos> listaEventos;//Arreglo donde se guardan los objetos Evento
    RecyclerView recyclerEvents;//Referencia al recycler view

    Activity activity;//Referencia a un activity que nos permite tener el contexto de la aplicacion
    ComunicaFragments icomunica;//Referencia a la interfaz

    private OnFragmentInteractionListener mListener;

    public destacados() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment destacados.
     */
    // TODO: Rename and change types and number of parameters
    public static destacados newInstance(String param1, String param2) {
        destacados fragment = new destacados();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //Aplicando el contexto a la interfaz para establecer la comunicación
        if(context instanceof Activity){
            this.activity = (Activity) context;
            icomunica = (ComunicaFragments) this.activity;
        }

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_destacados, container, false);

        //**ASIGNANDO EL RECYCLERVIEW AL FRAGMENTO**
        listaEventos = new ArrayList<>();
        recyclerEvents = vista.findViewById(R.id.recyclerID);
        recyclerEvents.setLayoutManager(new LinearLayoutManager(getContext()));

        llenarLista();//Llena el arreglo con los eventos que queremos mostrar

        AdapterEventos adapter = new AdapterEventos(listaEventos);

        //Accion al realizar cuando se detecta un evento en el elemento del recycler
        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Enviando el objeto evento al que se le esta dando click al fragment de los detalles
                icomunica.enviaEvento(listaEventos.get(recyclerEvents.getChildAdapterPosition(v)));
            }
        });
        recyclerEvents.setAdapter(adapter);

        return vista;
    }

    /**
     * Método que llena la lista de eventos, para posteriormente mostrarla
     */
    private void llenarLista(){
        listaEventos.add(new ListEventos(getString(R.string.Nombrecult2),getString(R.string.lugarCult2),getString(R.string.descCult2),getString(R.string.descComplCl2),getString(R.string.precioCult2),getString(R.string.fechaCult2),R.drawable.frida));
        listaEventos.add(new ListEventos(getString(R.string.Nombrecult3),getString(R.string.lugarCult3),getString(R.string.descCult3),getString(R.string.descComplCl3),getString(R.string.precioCult3),getString(R.string.fechaCult3),R.drawable.pagliacho));
        listaEventos.add(new ListEventos(getString(R.string.Nombrecult4),getString(R.string.lugarCult4),getString(R.string.descCult4),getString(R.string.descComplCl4),getString(R.string.precioCult4),getString(R.string.fechaCult4),R.drawable.inst));
        listaEventos.add(new ListEventos(getString(R.string.Nombreocio2),getString(R.string.lugarOcio2),getString(R.string.descocio2),getString(R.string.descComplocio2),getString(R.string.precioOcio2),getString(R.string.fechaOcio2),R.drawable.foo));
        listaEventos.add(new ListEventos(getString(R.string.Nombreocio4),getString(R.string.lugarOcio4),getString(R.string.descocio4),getString(R.string.descComplocio4),getString(R.string.precioOcio4),getString(R.string.fechaOcio4),R.drawable.macabro));
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}