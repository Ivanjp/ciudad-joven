package com.example.descubrecdmx.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.descubrecdmx.ListLugares;
import com.example.descubrecdmx.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.jetbrains.annotations.NotNull;

/**
 * Clase para dar estilo al fragmento que servira para mostrar la información de un lugar
 */
public class detalleLugar extends Fragment implements OnMapReadyCallback {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    //Variables para crear el mapa
    private GoogleMap mMap;
    private MapView mMapView;
    View vista;
    LatLng coord;//Coordenadas del lugar
    String nombre;//Nombre del lugar

    //Generando los componentes que se quiere mostrar
    TextView textNombre;//Nombre del lugar
    TextView textdireccion;//Dirección del lugar
    TextView textHorario;//Horario del lugar
    TextView textCosto;//Costo del lugar
    TextView textContacto;//Contacto del lugar
    TextView textDescripcion;//Descripcion del lugar
    ImageView imagen;//Imagen del lugar

    public detalleLugar() {
        // Required empty public constructor
    }



    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment detalleLugar.
     */
    // TODO: Rename and change types and number of parameters
    public static detalleLugar newInstance(String param1, String param2) {
        detalleLugar fragment = new detalleLugar();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }


    /**
    * Método donde se crea el fragmento con los datos del lugar que provienen de uno de los
    * elementos del recyclerview
    */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        vista = inflater.inflate(R.layout.fragment_detalle_lugar, container, false);

        //Dando las referencias del fragment a los componentes
        textNombre = vista.findViewById(R.id.nombreDetalle);
        textdireccion = vista.findViewById(R.id.direccDetalle);
        textHorario = vista.findViewById(R.id.HorarioDet);
        textCosto = vista.findViewById(R.id.costoDetalle);
        textContacto = vista.findViewById(R.id.contactoDet);
        textDescripcion = vista.findViewById(R.id.descDetLug);
        imagen = vista.findViewById(R.id.imagenDetalle);

        Bundle objetoLugar = getArguments();//Bundle para obtener el objeto
        ListLugares lugar = null;//Creando la instancia del objeto lugar

        //Cargando los datos del objeto
        if(objetoLugar != null){
            lugar = (ListLugares) objetoLugar.getSerializable("objeto");
            textNombre.setText(lugar.getNombre());
            textdireccion.setText(lugar.getDireccion());
            textHorario.setText(lugar.getHorarios());
            textCosto.setText(lugar.getCosto());
            textContacto.setText(lugar.getContacto());
            textDescripcion.setText(lugar.getDescripcion());
            imagen.setImageResource(lugar.getIdImage());
            coord = lugar.getCoord();
            nombre = lugar.getNombre();
        }

        return vista;
    }

    /**
     * Método para crear el mapa en el fragmento
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMapView = (MapView) view.findViewById(R.id.map);
        if(mMapView != null){
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }

    }


    /**
     * Método que inicializa el mapa
     * @param googleMap
     */
    @Override
    public void onMapReady(@NonNull @NotNull GoogleMap googleMap) {
        MapsInitializer.initialize(getContext());//Inicializando Mapa
        mMap = googleMap;

        //Añadiendo el marcador
        mMap.addMarker(new MarkerOptions()
                .position(coord)
                .title(nombre));
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);//Asignando el tipo de mapa
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coord,15));//Asignando la posicion y el zoom
    }

}