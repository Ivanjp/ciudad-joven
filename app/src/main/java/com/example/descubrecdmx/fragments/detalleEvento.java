package com.example.descubrecdmx.fragments;

import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.descubrecdmx.ListEventos;
import com.example.descubrecdmx.R;

/**
 * Clase para dar estilo al fragmento que servira para mostrar la información de un evento
 */
public class detalleEvento extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    //Generando los componentes que se quiere mostrar
    TextView textNombre;//Nombre del evento
    TextView textFecha;//Fecha del evento
    TextView textLugar;//Lugar del evento
    TextView textCosto;//Costo del evento
    TextView textDescripcion;//Descripción del evento
    ImageView imagen;//Imagen del evento

    public detalleEvento() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment detalleEvento.
     */
    // TODO: Rename and change types and number of parameters
    public static detalleEvento newInstance(String param1, String param2) {
        detalleEvento fragment = new detalleEvento();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    /**
    * Método donde se crea el fragmento con los datos del evento que provienen de uno de los
    * elementos del recyclerview
    */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_detalle_evento, container, false);

        //Dando las referencias del fragment a los componentes
        textNombre = vista.findViewById(R.id.idNombreE);
        textLugar = vista.findViewById(R.id.idLugarE);
        textFecha = vista.findViewById(R.id.idFechaE);
        textCosto = vista.findViewById(R.id.idCostoE);
        textDescripcion = vista.findViewById(R.id.idDescLargaE);
        imagen = vista.findViewById(R.id.idImagenE);

        Bundle objetoEvento = getArguments();//Bundle para obtener el objeto
        ListEventos evento = null;//Creando la instancia del objeto evento

        //Cargando los datos del objeto
        if(objetoEvento != null){
            evento = (ListEventos) objetoEvento.getSerializable("objeto");
            textNombre.setText(evento.getNombre());
            textLugar.setText(evento.getLugar());
            textFecha.setText(evento.getFecha());
            textCosto.setText(evento.getCosto());
            textDescripcion.setText(evento.getDescripcionLarga());
            imagen.setImageResource(evento.getIdImage());
        }

        return vista;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}