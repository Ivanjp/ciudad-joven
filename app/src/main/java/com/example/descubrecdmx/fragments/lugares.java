package com.example.descubrecdmx.fragments;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.descubrecdmx.AdapterEventos;
import com.example.descubrecdmx.AdapterLugares;
import com.example.descubrecdmx.ListEventos;
import com.example.descubrecdmx.ListLugares;
import com.example.descubrecdmx.R;
import com.example.descubrecdmx.interfaces.ComunicaFragments;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Fragmento para la sección de lugares
 */
public class lugares extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ArrayList<ListLugares> listaLugares;//Arreglo donde se guardan los objetos Lugar
    RecyclerView recyclerEvents;//Referencia al recycler view

    Activity activity;//Referencia a un activity que nos permite tener el contexto de la aplicacion
    ComunicaFragments icomunica;//Referencia a la interfaz

    private OnFragmentInteractionListener mListener;

    public lugares() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment lugares.
     */
    // TODO: Rename and change types and number of parameters
    public static lugares newInstance(String param1, String param2) {
        lugares fragment = new lugares();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //Aplicando el contexto a la interfaz para establecer la comunicación
        if(context instanceof Activity){
            this.activity = (Activity) context;
            icomunica = (ComunicaFragments) this.activity;
        }

        if (context instanceof destacados.OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_lugares, container, false);

        //**ASIGNANDO EL RECYCLERVIEW AL FRAGMENTO**
        listaLugares = new ArrayList<>();
        recyclerEvents = vista.findViewById(R.id.recyclerIDLugares);
        recyclerEvents.setLayoutManager(new LinearLayoutManager(getContext()));

        llenarLista();//Llena el arreglo con los lugares que queremos mostrar

        AdapterLugares adapter = new AdapterLugares(listaLugares);
        //Accion al realizar cuando se detecta un evento en el elemento del recycler

        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Enviando el objeto evento al que se le esta dando click al fragment de los detalles
                icomunica.enviaLugar(listaLugares.get(recyclerEvents.getChildAdapterPosition(v)));
            }
        });
        recyclerEvents.setAdapter(adapter);

        return vista;
    }


    /**
     * Método que llena la lista de lugares, para posteriormente mostrarla
     */
    private void llenarLista(){
        listaLugares.add(new ListLugares(getString(R.string.Nombrelug1),getString(R.string.direccLug1),getString(R.string.horarioLug1),getString(R.string.costoLug1),getString(R.string.contactLug1),getString(R.string.descLug1),R.drawable.parque,new LatLng(19.41159150586387, -99.16953551680673)));
        listaLugares.add(new ListLugares(getString(R.string.Nombrelug2),getString(R.string.direccLug2),getString(R.string.horarioLug2),getString(R.string.costoLug2),getString(R.string.contactLug2),getString(R.string.descLug2),R.drawable.azulejos, new LatLng(19.43454105517713, -99.14021664748931)));
        listaLugares.add(new ListLugares(getString(R.string.Nombrelug3),getString(R.string.direccLug3),getString(R.string.horarioLug3),getString(R.string.costoLug3),getString(R.string.contactLug3),getString(R.string.descLug3),R.drawable.tezo, new LatLng(19.500061755819225, -99.21082910515904)));
        listaLugares.add(new ListLugares(getString(R.string.Nombrelug4),getString(R.string.direccLug4),getString(R.string.horarioLug4),getString(R.string.costoLug4),getString(R.string.contactLug4),getString(R.string.descLug4),R.drawable.auto2,new LatLng(19.32504979997498, -99.13922876931703)));
        listaLugares.add(new ListLugares(getString(R.string.Nombrelug5),getString(R.string.direccLug5),getString(R.string.horarioLug5),getString(R.string.costoLug5),getString(R.string.contactLug5),getString(R.string.descLug5),R.drawable.luz2,new LatLng(19.43641166559878, -99.130052774477)));
        listaLugares.add(new ListLugares(getString(R.string.Nombrelug6),getString(R.string.direccLug6),getString(R.string.horarioLug6),getString(R.string.costoLug6),getString(R.string.contactLug6),getString(R.string.descLug6),R.drawable.pol,new LatLng(19.393449128020777, -99.17337557447775)));

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}